const pkg = require('./package');

const title = 'Itential for Developers | Drive Automation for the Modern Networks';

module.exports = {
  mode: 'universal',
  // Headers of the page
  head: {
    title,
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description },
      { hid: 'twitter:description', name: 'twitter:description', content: pkg.description },
      { hid: 'twitter:card', name: 'twitter:card', content: 'summary_large_image' },
      { hid: 'twitter:title', name: 'twitter:title', content: title },
      { hid: 'twitter:image', name: 'twitter:image', content: 'https://developer.itential.io/meta.jpg' },
      { hid: 'og:description', name: 'og:description', content: pkg.description },
      { hid: 'og:locale', property: 'og:locale', content: 'en_US' },
      { hid: 'og:type', property: 'og:type', content: 'website' },
      { hid: 'og:title', property: 'og:title', content: title },
      { hid: 'og:url', property: 'og:url', content: 'https://developer.itential.io/' },
      { hid: 'og:site_name', property: 'og:site_name', content: 'Itential for Developers' },
      { hid: 'og:image', property: 'og:image', content: 'https://developer.itential.io/meta.jpg' },
      { hid: 'og:image:secure_url', property: 'og:image:secure_url', content: 'https://developer.itential.io/meta.jpg' },
      { hid: 'og:image:width', property: 'og:image:width', content: '720' },
      { hid: 'og:image:height', property: 'og:image:height', content: '390' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'canonical', href: 'https://developer.itential.io/' },
    ],
  },
  // Customize the generated output folder
  generate: {
    dir: 'public',
  },
  // Global CSS
  css: [
    '@/assets/css/main.css',
  ],
  // Plugins to load before mounting the App
  plugins: [
    { src: '~plugins/munchkin', ssr: false },
    { src: '~plugins/leadlander', ssr: false },
    // { src: '~/plugins/marketoform', ssr: false },
  ],
  // Nuxt.js modules
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    // Doc: https://buefy.github.io/#/documentation
    ['nuxt-buefy', {
      materialDesignIcons: false,
    }],
    ['@nuxtjs/google-analytics', {
      id: 'UA-58410434-2',
    }],
    'nuxt-compress',
  ],
  // Axios module configuration
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },
  // Build configuration
  build: {
    // You can extend webpack config here
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
        });
      }
    },
  },
};
