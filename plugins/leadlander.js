
/* eslint-disable */
export default ({ app }) => {
  // only run on client-side and in prod mode
  if (process.env.NODE_ENV !== 'production') return;
  window.sf14gv = 32230;
  (function () {
    var sf14g = document.createElement('script');
    sf14g.type = 'text/javascript';
    sf14g.src = 'https://tracking.leadlander.com/lt.min.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(sf14g, s);
  })();
};
